#ifndef SRC_CALCUL_H_
#define SRC_CALCUL_H_

#ifdef __cplusplus
extern "C" {
#endif

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <math.h>

    #define OK 1
    #define Errorr 0  // Empty stack or error
    #define Fail "Error"

    struct node{
        char *data;
        struct node* link;
    };
    typedef struct node stack;

    typedef struct matrix_struct {
        char** matrix;
        int rows;
        int columns;
    } matrix_t;

    // Stack funcs
    void push(char x[], stack** top);
    int display(stack** top);
    int pop(stack** top, char* result);
    int peek(stack** top, char* result);

    // Work with matrix for grouping symbols
    int s21_create_matrix(int rows, int columns, matrix_t *result);
    void s21_remove_matrix(matrix_t *A);
    void filter_list_to_matrix(char expression[], matrix_t *result);

    // Calculation
    char* calculation(matrix_t* matrix_groupe);
    int result(stack** top_numbers, stack** top_symbols, int flag_trigonometry);
    void clean_stacks(stack** top_numbers, stack** top_symbols);

#ifdef __cplusplus
}
#endif

#endif  // SRC_CALCUL_H_
