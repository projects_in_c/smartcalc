#include "calcul.h"

int main() {
    matrix_t matrix_group;
    char* str = "1.25+3.45";
    s21_create_matrix(10, 7, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    char* buff = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    printf("%s", buff);
    free(buff);

    str = "0.003-15.34";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    char* actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "0.25*5";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);
    return 0;

    str = "0.2/5";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "60mod3.5";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "5^3.45";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "(5.23+1.25)*(0.25+0.001)";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "sin(0.3*0.6)";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "cos(0.6-0.1+0.2/0.5)";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "tan(0.8/2)";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "sqrt(97.3456)";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "ln(2.5+99)";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "log(1.8+256.34)";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "asin(0.34)";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);
    str = "acos(-0.83)";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "atan(-0.669)";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "1-1";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "1-2-3";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "4*(-5)+2.01/sin(0.01)";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "sin(0.1)";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "cos(sin(tan(-0.005)))";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);

    str = "125/0";
    s21_create_matrix(10, 15, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);
    actual = calculation(&matrix_group);
    s21_remove_matrix(&matrix_group);
    free(actual);
}
