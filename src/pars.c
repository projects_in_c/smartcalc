#include "calcul.h"

/*============================================================
                STACK: push, pop, peek, display
============================================================*/
/*---------------------------
Put value into container
----------------------------*/
void push(char x[], stack** top) {
    stack* newnode;
    newnode = (stack*) malloc(sizeof(stack));
    newnode -> data = (char*) malloc(strlen(x) * sizeof(char));
    strcpy(newnode -> data, x);
    // snprintf(newnode -> data, sizeof(newnode -> data) * strlen(x), x);
    newnode -> link = *top;
    *top = newnode;
}

/*---------------------------
Show values of container
----------------------------*/
int display(stack** top) {
    stack* temp;
    temp = *top;
    if (*top == 0) {
        return Errorr;
    } else {
        while (temp != 0) {
           printf("%s\n", temp -> data);
           temp = temp -> link;
        }
        return OK;
    }
}

/*---------------------------
See top of the elements of container 
----------------------------*/
int peek(stack** top, char* result) {
    if (*top == 0) {
        return Errorr;
    } else {
        strcpy(result, (*top) -> data);
        return OK;
    }
}

/*---------------------------
take top of the elements of container and delet it
----------------------------*/
int pop(stack** top, char* result) {
    stack* temp;
    temp = *top;
    if (*top == 0) {
        return Errorr;
    } else {
        strcpy(result, (*top) -> data);
        free((*top) -> data);
        (*top) -> data = NULL;
        if ((*top) -> link == 0) {
            free((*top));
            (*top) = NULL;
        } else {
            temp = (*top) -> link;
            free((*top));
            (*top) = NULL;
            *top = temp;
        }
        return OK;
    }
}


/*============================================================
                PARS:
============================================================*/
/*---------------------------
    Создание матриц (create_matrix)
----------------------------*/
int s21_create_matrix(int rows, int columns, matrix_t *result) {
    if (rows < 1 || columns < 1) {
        return Errorr;
    }
    result -> rows = rows;
    result -> columns = columns;
    result -> matrix = (char**) malloc(sizeof(char*) * rows);
    for (int i = 0; i < rows; i++) {
        result -> matrix[i] = (char*)malloc(sizeof(char*) * columns);
    }
    return  OK;
}

/*---------------------------
    Очистка матриц (remove_matrix)
----------------------------*/
void s21_remove_matrix(matrix_t *A) {
    for (int i = 0; i < A->rows; i++) {
        free(A->matrix[i]);
    }
    free(A -> matrix);
    A -> matrix = NULL;
}

/*---------------------------
    Группировка значений на числа, тригонометрические функции (sin, cos, т.д.)
----------------------------*/
void filter_list_to_matrix(char expression[], matrix_t *result) {
    int j = 0;
    int flag_0 = 0;
    int flag_1 = 0;
    int num_column = 0;
    int count_len_row_matrix = 0;
    for (int i = 0; i < (int)strlen(expression); i++) {
        // Проверяем, если строки заполнились расширяем место для матрицы
        if ((count_len_row_matrix+1) > (result -> rows - 1)) {
            result -> rows += 10;
            result -> matrix = (char**)realloc(result -> matrix, sizeof(char*) * result -> rows);
            for (int i = 10; i > 0; i--) {
                result -> matrix[result -> rows - i] = (char*)malloc(sizeof(char*) * result -> columns);
            }
        }
        // Если число
        if (expression[i] == '0' || expression[i] == '1' || expression[i] == '2' ||
            expression[i] == '3' || expression[i] == '4' || expression[i] == '5' ||
            expression[i] == '6' || expression[i] == '7' || expression[i] == '8' ||
            expression[i] == '9' || expression[i] == '.') {
            if (flag_1 == 1) {
                j++;
                flag_1 = 0;
                num_column = 0;
                count_len_row_matrix += 1;
            }
            result -> matrix[j][num_column] = expression[i];
            flag_0 = 1;
            num_column += 1;
        // Если буква
        } else if ((int)expression[i] > 96 && (int)expression[i] < 123) {
            if (flag_0 == 1) {
                j++;
                flag_0 = 0;
                num_column = 0;
                count_len_row_matrix += 1;
            }
            result -> matrix[j][num_column] = expression[i];
            flag_1 = 1;
            num_column += 1;
        // Если математический оператор
        } else if (expression[i] == '-' || expression[i] == '+' || expression[i] == '*' ||
            expression[i] == '/' || expression[i] == '(' || expression[i] == ')' ||
            expression[i] == '(' || expression[i] == '^') {
            if (flag_1 == 1 || flag_0 == 1) {
                num_column = 0;
                j++;
                count_len_row_matrix += 1;
                flag_0 = 0;
                flag_1 = 0;
            }
            result -> matrix[j][num_column] = expression[i];
            count_len_row_matrix += 1;
            j++;
        }
    }
}

/*---------------------------
    Приоритет:  +:   1    Отдельно: sin, cos, tan,
                -:   1              asin, acos, atan,
                *:   2              ln, log, '(', ')';      
                /:   2
                mod: 2
                ^:   3
----------------------------*/
char* calculation(matrix_t* matrix_groupe) {
    stack* top_numbers = 0;
    stack* top_symbols = 0;
    char symbol[7] = " ";
    for (int i = 0; i < matrix_groupe -> rows; i++) {
        // if numbers, push to stack for numbers
        if (*(matrix_groupe -> matrix[i]) > 47 && *(matrix_groupe -> matrix[i]) < 58) {
            push(matrix_groupe -> matrix[i], &top_numbers);
        // Если буквы и '('
        } else if ((*(matrix_groupe -> matrix[i]) > 96 && *(matrix_groupe -> matrix[i]) < 109) ||
                    (*(matrix_groupe -> matrix[i]) > 109 && *(matrix_groupe -> matrix[i]) < 127) ||
                    *(matrix_groupe -> matrix[i]) == 40) {
            push(matrix_groupe -> matrix[i], &top_symbols);
        // Если + || -
        } else if (*(matrix_groupe -> matrix[i]) == 43 || *(matrix_groupe -> matrix[i]) == 45) {
            // char number
            if (top_symbols != 0) {
                peek(&top_symbols, symbol);
                if (*symbol == 42 || *symbol == 47 || *symbol == 94 || *symbol == 43 \
                    || *symbol == 45 || *symbol == 109) {
                    while (*symbol == 42 || *symbol == 47 || *symbol == 94 || *symbol == 43 \
                        || *symbol == 45 || *symbol == 109) {
                        if (!result(&top_numbers, &top_symbols, 0)) {
                            clean_stacks(&top_numbers, &top_symbols);
                            char* number_result = (char*) malloc(6 * sizeof(char));
                            strcpy(number_result, Fail);
                            return number_result;
                        }
                        strcpy(symbol, " ");
                        peek(&top_symbols, symbol);
                    }
                    push(matrix_groupe -> matrix[i], &top_symbols);
                // Если встретился минус и он стоит псоле открывающей скобки -> изменить число на минус
                } else if (i > 1 && *(matrix_groupe -> matrix[i]) == 45 && \
                    *(matrix_groupe -> matrix[i-1]) == 40) {
                    strcat(matrix_groupe -> matrix[i], matrix_groupe -> matrix[i+1]);
                    push(matrix_groupe -> matrix[i], &top_numbers);
                    i++;
                } else {
                    push(matrix_groupe -> matrix[i], &top_symbols);
                }
            } else {
                push(matrix_groupe -> matrix[i], &top_symbols);
            }
        // Если * || / || mod
        } else if (*(matrix_groupe -> matrix[i]) == 42 || *(matrix_groupe -> matrix[i]) == 47 \
            || *(matrix_groupe -> matrix[i]) == 109) {
            if (top_symbols != 0) {
                peek(&top_symbols, symbol);
                if (*symbol == 94 || *symbol == 42 || *symbol == 47 || *symbol == 109) {
                    while (*symbol == 94 || *symbol == 42 || *symbol == 47 || *symbol == 109) {
                        if (!result(&top_numbers, &top_symbols, 0)) {
                            clean_stacks(&top_numbers, &top_symbols);
                            char* number_result = (char*) malloc(6 * sizeof(char));
                            strcpy(number_result, Fail);
                            return number_result;
                        }
                        strcpy(symbol, " ");
                        peek(&top_symbols, symbol);
                    }
                    push(matrix_groupe -> matrix[i], &top_symbols);
                } else {
                    push(matrix_groupe -> matrix[i], &top_symbols);
                }
            } else {
                push(matrix_groupe -> matrix[i], &top_symbols);
            }
        // Если  ^
        } else if (*(matrix_groupe -> matrix[i]) == 94) {
            peek(&top_symbols, symbol);
            if (*symbol == 94) {
                if (!result(&top_numbers, &top_symbols, 0)) {
                    clean_stacks(&top_numbers, &top_symbols);
                    char* number_result = (char*) malloc(6 * sizeof(char));
                    strcpy(number_result, Fail);
                    return number_result;
                }
                push(matrix_groupe -> matrix[i], &top_symbols);
                strcpy(symbol, " ");
            } else {
                push(matrix_groupe -> matrix[i], &top_symbols);
            }
        //  Если ')'
        } else if (*(matrix_groupe -> matrix[i]) == 41) {
            peek(&top_symbols, symbol);
            while (*symbol != 40) {
                if (!result(&top_numbers, &top_symbols, 0)) {
                    clean_stacks(&top_numbers, &top_symbols);
                    char* number_result = (char*) malloc(6 * sizeof(char));
                    strcpy(number_result, Fail);
                    return number_result;
                }
                peek(&top_symbols, symbol);
            }
            pop(&top_symbols, symbol);
            if (top_symbols != 0) {
                peek(&top_symbols, symbol);
                if ((*symbol > 96 && *symbol < 109) || (*symbol > 109 && *symbol < 127)) {
                    if (!result(&top_numbers, &top_symbols, 1)) {
                        char* number_result = (char*) malloc(6 * sizeof(char));
                        strcpy(number_result, Fail);
                        return number_result;
                    }
                }
            }
        }
    }
    while (top_symbols != 0) {
        if (!result(&top_numbers, &top_symbols, 0)) {
            clean_stacks(&top_numbers, &top_symbols);
            char* number_result = (char*) malloc(6 * sizeof(char));
            strcpy(number_result, Fail);
            return number_result;
        }
    }
    char* number_result = (char*) malloc(strlen(top_numbers -> data) * sizeof(char));
    pop(&top_numbers, number_result);
    return number_result;
}

int result(stack** top_numbers, stack** top_symbols, int flag_trigonometry) {
    double number_float = 0;
    unsigned long long number_int = 0;
    int length_after_p = 0;

    long double out = 0;
    char symbol[7] = " ";
    char *e;

    if (flag_trigonometry == 0) {
        // Обработка когда есть минус перед числом в самом конце стека
        if ((*top_numbers) -> link == 0) {
            char* number_1 = (char*) malloc(strlen((*top_numbers) -> data) * sizeof(char));
            pop(top_symbols, symbol);
            pop(top_numbers, number_1);
            out = strtod(number_1, &e) * -1.0;
            free(number_1);
            number_1 = NULL;
        } else {
            char* number_1 = (char*) malloc(strlen((*top_numbers) -> data) * sizeof(char));
            char* number_2 = (char*) malloc(strlen((*top_numbers) -> link -> data) * sizeof(char));

            pop(top_symbols, symbol);
            pop(top_numbers, number_1);
            pop(top_numbers, number_2);
            if (symbol[0] == '+') {
                out = strtold(&number_2[0], 0) + strtold(&number_1[0], 0);
            } else if (symbol[0] == '-') {
                out = strtold(&number_2[0], 0) - strtold(&number_1[0], 0);
            } else if (symbol[0] == '*') {
                out = strtold(&number_2[0], 0) * strtold(&number_1[0], 0);
            } else if (symbol[0] == '/') {
                out = strtold(&number_2[0], 0) / strtold(&number_1[0], 0);
            } else if (symbol[0] == '^') {
                out =  pow(atof(&number_2[0]), atof(&number_1[0]));
            } else if (symbol[0] == 'm') {
                out = fmod(atof(&number_2[0]), atof(&number_1[0]));
            }
            free(number_1);
            free(number_2);
            number_1 = NULL;
            number_2 = NULL;
        }
    } else {
        char* number_1 = (char*) malloc(strlen((*top_numbers) -> data) * sizeof(char));
        pop(top_symbols, symbol);
        pop(top_numbers, number_1);
        if (symbol[0] == 's' && symbol[1] == 'i') {
            out = sin(strtod(number_1, &e));
        } else if (symbol[0] == 'c' && symbol[1] == 'o') {
            out = cos(strtod(number_1, &e));
        } else if (symbol[0] == 't') {
            out = tan(strtod(number_1, &e));
        } else if (symbol[0] == 'a' && symbol[1] == 'c') {
            out = acos(strtod(number_1, &e));
        } else if (symbol[0] == 'a' && symbol[1] == 's') {
            out = asin(strtod(number_1, &e));
        } else if (symbol[0] == 'a' && symbol[1] == 't') {
            out = atan(strtod(number_1, &e));
        } else if (symbol[0] == 's' && symbol[1] == 'q') {
            out = sqrt(strtod(number_1, &e));
        } else if (symbol[0] == 'l' && symbol[1] == 'n') {
            out = log(strtod(number_1, &e));
        } else if (symbol[0] == 'l' && symbol[1] == 'o') {
            out = log10l(strtold(number_1, &e));
        }
        free(number_1);
        number_1 = NULL;
    }
    // Расчитываем длину после точки, чтобы при переводе в str знать сколько переводить
    if (isnan(out) || isinf(out)) {
        return Errorr;
    }
    long double out_pos = 0;
    if (out < 0) {
        out_pos = out * -1;
    } else {
        out_pos = out;
    }
    number_int = out_pos;
    number_float = out_pos;
    while (number_float - (double)number_int != 0.0 && \
    !((number_float - (double)number_int) < 0.0)) {
        number_float = out_pos * pow(10, length_after_p + 1);
        number_int = number_float;
        length_after_p++;
    }
    // переводим float в str и кладем в стек
    char arr[sizeof(out)];
    snprintf(arr, sizeof(out), "%.*Lf", length_after_p, out);
    push(arr, top_numbers);
    return OK;
}

void clean_stacks(stack** top_numbers, stack** top_symbols) {
    char symbol[7] = " ";
    while (*top_symbols != 0) {
        pop(top_symbols, symbol);
    }
    while (*top_numbers != 0) {
        pop(top_numbers, symbol);
    }
}
