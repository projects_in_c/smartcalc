#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QString>
#include <QVector>
#include <QTimer>
#include <QtMath>
#include <QDebug>

extern "C" {
    #include "./../calcul.h"
}

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    double xBegin, xEnd, h, X;
    int N;

    QVector<double> x, y, buff;
    QTimer *timer;
    int time;

private slots:
    void digits_numbers();
    void on_pushButton_dot_clicked();
    void operations();

    void on_pushButton_ac_clicked();
    void on_pushButton_del_clicked();

    void simple_operation_clicked();
    void brackets_clicked();
    void trigonometry_clicked();

    void calculation_qt();
    void build_graph();
    QString calc_graph_y();
//    void on_pushButton_x_clicked();

    void TimerSlot();
};
#endif // MAINWINDOW_H
