#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->pushButton_0, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui->pushButton_1, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui->pushButton_4, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui->pushButton_5, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui->pushButton_6, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui->pushButton_7, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui->pushButton_8, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui->pushButton_9, SIGNAL(clicked()), this, SLOT(digits_numbers()));
    connect(ui->pushButton_plus_minus, SIGNAL(clicked()), this, SLOT(operations()));

    connect(ui->pushButton_divide, SIGNAL(clicked()), this, SLOT(simple_operation_clicked()));
    connect(ui->pushButton_multiply, SIGNAL(clicked()), this, SLOT(simple_operation_clicked()));
    connect(ui->pushButton_minus, SIGNAL(clicked()), this, SLOT(simple_operation_clicked()));
    connect(ui->pushButton_plus, SIGNAL(clicked()), this, SLOT(simple_operation_clicked()));
    connect(ui->pushButton_mod, SIGNAL(clicked()), this, SLOT(simple_operation_clicked()));
    connect(ui->pushButton_degree, SIGNAL(clicked()), this, SLOT(simple_operation_clicked()));

    connect(ui->pushButton_bracket_left, SIGNAL(clicked()), this, SLOT(brackets_clicked()));
    connect(ui->pushButton_bracket_right, SIGNAL(clicked()), this, SLOT(brackets_clicked()));

    connect(ui->pushButton_cos, SIGNAL(clicked()), this, SLOT(trigonometry_clicked()));
    connect(ui->pushButton_acos, SIGNAL(clicked()), this, SLOT(trigonometry_clicked()));
    connect(ui->pushButton_sin, SIGNAL(clicked()), this, SLOT(trigonometry_clicked()));
    connect(ui->pushButton_asin, SIGNAL(clicked()), this, SLOT(trigonometry_clicked()));
    connect(ui->pushButton_tan, SIGNAL(clicked()), this, SLOT(trigonometry_clicked()));
    connect(ui->pushButton_atan, SIGNAL(clicked()), this, SLOT(trigonometry_clicked()));
    connect(ui->pushButton_sqrt, SIGNAL(clicked()), this, SLOT(trigonometry_clicked()));
    connect(ui->pushButton_ln, SIGNAL(clicked()), this, SLOT(trigonometry_clicked()));
    connect(ui->pushButton_log, SIGNAL(clicked()), this, SLOT(trigonometry_clicked()));

    connect(ui->pushButton_equal, SIGNAL(clicked()), this, SLOT(calculation_qt()));
    connect(ui->pushButton_graph, SIGNAL(clicked()), this, SLOT(build_graph()));
    connect(ui->pushButton_x, SIGNAL(clicked()), this, SLOT(digits_numbers()));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::digits_numbers() {
    QPushButton *button = (QPushButton*)sender();
    int size = ui->show_result->text().size();
    if ((size == 1) & (ui->show_result->text() == "0")) {
        ui->show_result->setText(button -> text());
    } else {
        QString new_label;
        new_label = (ui->show_result->text() + button -> text());
        ui->show_result->setText(new_label);
    }
}


void MainWindow::operations() {
    QPushButton *button = (QPushButton*)sender();
    double all_numbers;
    QString new_label;
    int i = 0;

    if (button->text() == "+/-") {
        QByteArray ba = ui->show_result->text().toLocal8Bit();
        char* str = ba.data();
        matrix_t matrix_group;
        s21_create_matrix(10, 7, &matrix_group);
        filter_list_to_matrix(str, &matrix_group);

        while(*(matrix_group.matrix[i]) > 39 && *(matrix_group.matrix[i]) < 126) {
            i++;
        }

        if (i == 1) {
            all_numbers = (ui->show_result->text()).toDouble();
            all_numbers = all_numbers * -1;
            new_label = QString::number(all_numbers, 'g', 15);
            ui->show_result->setText(new_label);
        } else if (*(matrix_group.matrix[0]) == 45 && *(matrix_group.matrix[1]) == 40) {
            QString str(ui->show_result->text());
            ui->show_result->setText(str.mid(1, -1));
        } else {
            ui->show_result->setText("-(" + ui->show_result->text() + ')');
        }
        s21_remove_matrix(&matrix_group);
    }
}


void MainWindow::on_pushButton_dot_clicked()
{
    QString str(ui->show_result->text());
    int size = ui->show_result->text().size();
    if (str[size-1] != QString(".")[0]) {
        ui -> show_result->setText(ui -> show_result->text() + ".");
    }
}


void MainWindow::on_pushButton_ac_clicked()
{
    ui->show_result->setText("0");
}

void MainWindow::on_pushButton_del_clicked()
{
    int size = ui->show_result->text().size();
    if (size == 1) {
        ui->show_result->setText("0");
    } else {
        ui->show_result->setText(ui -> show_result->text().remove(size-1, size));
    }
}


void MainWindow::simple_operation_clicked() {
    QPushButton *button = (QPushButton*)sender();
    QString new_label;
    QString old_label = ui->show_result->text();

    int size = ui->show_result->text().size();

    if (old_label.endsWith('-') || old_label.endsWith('+') || old_label.endsWith('/') ||
        old_label.endsWith("mod") || old_label.endsWith('*') || old_label.endsWith('^')) {
        if (old_label.endsWith("mod")) {
            old_label.replace(size-3, 3, button->text());
        } else {
            old_label.replace(size-1, 1, button->text());
        }
        ui->show_result->setText(old_label);
    } else {
        new_label = (ui->show_result->text() + button -> text());
        ui->show_result->setText(new_label);
    }
}


void MainWindow::brackets_clicked()
{
    QPushButton *button = (QPushButton*)sender();
    QString new_label;
    QString old_label = ui->show_result->text();

    if (button -> text() == "(") {
        if (old_label == '0') {
            new_label = (button -> text());
            ui->show_result->setText(new_label);
        } else if (old_label.endsWith('-') || old_label.endsWith('+') || old_label.endsWith('/') ||
            old_label.endsWith("mod") || old_label.endsWith('*') || old_label.endsWith('^')) {
            new_label = (ui->show_result->text() + button -> text());
            ui->show_result->setText(new_label);
        } else {
            new_label = (ui->show_result->text() + "*" + button -> text());
            ui->show_result->setText(new_label);
        }
    } else {
        new_label = (ui->show_result->text() + button -> text());
        ui->show_result->setText(new_label);
    }
}


void MainWindow::trigonometry_clicked()
{
    QPushButton *button = (QPushButton*)sender();
    QString new_label;
    QString old_label = ui->show_result->text();

    if (old_label.endsWith('-') || old_label.endsWith('+') || old_label.endsWith('/') ||
        old_label.endsWith("mod") || old_label.endsWith('*') || old_label.endsWith('^') ||
        old_label.endsWith("(")) {
            new_label = (ui->show_result->text() + button -> text() + "(");
            ui->show_result->setText(new_label);
    } else if (old_label == '0') {
        new_label = (button -> text() + "(");
        ui->show_result->setText(new_label);
    } else {
        new_label = (old_label + "*" + button -> text() + "(");
        ui->show_result->setText(new_label);
    }
}

void MainWindow::calculation_qt()
{
    QString input = ui -> show_result -> text();
    std::string str = input.toStdString();
    char* cstr = new char[str.length() + 1];

    strcpy(cstr, str.c_str());

    matrix_t matrix_group;
    s21_create_matrix(10, 7, &matrix_group);
    filter_list_to_matrix(cstr, &matrix_group);

    char* buff_1 = calculation(&matrix_group);
    std::string res_str(buff_1);
    QString out = QString::fromStdString(res_str);
    qDebug() << out;

    ui->show_result->setText(out);
    s21_remove_matrix(&matrix_group);
    free(buff_1);
}

void MainWindow::build_graph()
{
    h = 0.1;
    xBegin = ui->Xmin->text().toDouble();
    xEnd = ui->Xmax->text().toDouble() + h;
    N = (xEnd - xBegin) / h + 2;

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(TimerSlot()));
    ui->widget->clearGraphs();
    timer->start(20);
    X=xBegin;
    x.clear();
    y.clear();
}

void MainWindow::TimerSlot()
{
    if(time <= 20 * N) {
        if(X <= xEnd) {
            QString z = calc_graph_y();
            X = QString::number(X,'f', 1).toDouble();
            if (z != "Error") {
                double buff = z.toDouble();
                buff = QString::number(buff,'f', 2).toDouble();
                x.push_back(X);
                y.push_back(buff);
            } else {
                x.push_back(qQNaN());
                y.push_back(qQNaN());
            }
            qDebug() << X;
            X += h;
        }
        time += 20;
    } else {
        time = 0;
        timer -> stop();
    }
    ui->widget->addGraph();
    ui->widget->graph(0)->addData(x,y);
    ui->widget->rescaleAxes();
    ui->widget->replot();

    ui -> widget -> setInteraction(QCP::iRangeZoom, true);
    ui -> widget -> setInteraction(QCP::iRangeDrag, true);
}

QString MainWindow::calc_graph_y()
{
    QString line = ui->show_result->text();
    while (true) {
        QString str_x;
        int index = line.indexOf('X');
        if (index > -1) {
            if (X < 0) {
                str_x = "(" + QString::number(X) + ")";
            } else {
                str_x = QString::number(X);
            }
            line.replace(index, 1, str_x);
//            qDebug() << line;
        } else {
            break;
        }
    }
    QByteArray ba = line.toLocal8Bit();
    char* str = ba.data();
    matrix_t matrix_group;

    s21_create_matrix(10, 7, &matrix_group);
    filter_list_to_matrix(str, &matrix_group);

    char* buff_1 = calculation(&matrix_group);
    std::string res_str(buff_1);
    QString out = QString::fromStdString(res_str);
    s21_remove_matrix(&matrix_group);
    free(buff_1);

    return out;
}

